extern crate cpython;
extern crate ndarray;

mod player;

use std::mem;

use cpython::{py_fn, py_module_initializer, PyDict, PyResult, PyTuple, Python, ToPyObject};
use ndarray::{
    azip, s, Array6, ArrayD, ArrayView1, Ix4, Ix5, Ix6, IxDyn, SliceInfo, SliceInfoElem,
};
use player::Player;

py_module_initializer!(lineup, |py, m| {
    m.add(py, "__doc__", "Rust module for selecting fanduel lineups")?;
    m.add(
        py,
        "select_best_lineup",
        py_fn!(
            py,
            select_best_lineup(
                players: Vec<Player>,
                max_salary: usize,
                pos_counts: PosCounts
            )
        ),
    )?;

    Ok(())
});

type PosCounts = Vec<i32>;

fn scs(elems: Vec<SliceInfoElem>) -> SliceInfo<[SliceInfoElem; 6], Ix6, Ix6> {
    let arr: [SliceInfoElem; 6] = elems.try_into().expect("couldn't convert to array");
    SliceInfo::try_from(arr).expect("failed to convert to sliceInfo")
}

fn ps(mut elems: Vec<SliceInfoElem>) -> SliceInfo<[SliceInfoElem; 7], IxDyn, IxDyn> {
    elems.push(SliceInfoElem::from(..));
    let arr: [SliceInfoElem; 7] = elems.try_into().expect("couldn't convert to array");
    SliceInfo::try_from(arr).expect("failed to convert to sliceInfo")
}

fn single_slice(elems: Vec<SliceInfoElem>) -> SliceInfo<[SliceInfoElem; 6], Ix6, Ix5> {
    let arr: [SliceInfoElem; 6] = elems.try_into().expect("couldn't convert to array");
    SliceInfo::try_from(arr).expect("failed to convert to sliceInfo")
}

fn single_slice2(elems: &[SliceInfoElem]) -> SliceInfo<[SliceInfoElem; 6], Ix6, Ix4> {
    let arr: [SliceInfoElem; 6] = elems.try_into().expect("couldn't convert to array");
    SliceInfo::try_from(arr).expect("failed to convert to sliceInfo")
}

fn take_slice(salary: i32, pos: usize) -> Vec<SliceInfoElem> {
    let mut elems = vec![SliceInfoElem::from(0..-salary)];
    for p in 0..5 {
        elems.push(if p == pos {
            SliceInfoElem::from(0..-1)
        } else {
            SliceInfoElem::from(..)
        });
    }
    elems
}

fn take_slice2(salary: i32, pos: usize, pos2: usize) -> Vec<SliceInfoElem> {
    let mut elems = vec![SliceInfoElem::from(0..-salary)];
    for p in 0..5 {
        elems.push(if p == pos {
            SliceInfoElem::from(0..-1)
        } else if p == pos2 {
            SliceInfoElem::from(1..)
        } else {
            SliceInfoElem::from(..)
        });
    }
    elems
}

fn leave_slice(salary: i32, pos: usize) -> Vec<SliceInfoElem> {
    let mut elems = vec![SliceInfoElem::from(salary..)];
    for p in 0..5 {
        elems.push(if p == pos {
            SliceInfoElem::from(1..)
        } else {
            SliceInfoElem::from(..)
        });
    }
    elems
}

fn leave_slice2(salary: i32, pos: usize, pos2: usize) -> Vec<SliceInfoElem> {
    let mut elems = vec![SliceInfoElem::from(salary..)];
    for p in 0..5 {
        elems.push(if p == pos || p == pos2 {
            SliceInfoElem::from(1..)
        } else {
            SliceInfoElem::from(..)
        });
    }
    elems
}

fn copy_slice(salary: i32, pos: usize) -> SliceInfo<[SliceInfoElem; 6], Ix6, Ix5> {
    let mut elems = vec![SliceInfoElem::from(salary..)];
    for p in 0..5 {
        elems.push(if p == pos {
            SliceInfoElem::Index(0)
        } else {
            SliceInfoElem::from(..)
        });
    }
    single_slice(elems)
}

fn copy_slice2(salary: i32, pos: usize, pos2: usize) -> SliceInfo<[SliceInfoElem; 6], Ix6, Ix4> {
    let mut elems = vec![SliceInfoElem::from(salary..)];
    for p in 0..5 {
        elems.push(if p == pos || p == pos2 {
            SliceInfoElem::Index(0)
        } else {
            SliceInfoElem::from(..)
        });
    }
    single_slice2(elems.as_slice())
}

struct LineupArray {
    scores: Array6<f32>,
    players: ArrayD<i32>,
}

#[derive(Debug, Clone, PartialEq)]
struct OutputPlayer {
    id: i32,
    pos: i32,
}

impl ToPyObject for OutputPlayer {
    type ObjectType = PyDict;

    fn to_py_object(&self, py: Python) -> Self::ObjectType {
        let dict = PyDict::new(py);
        dict.set_item(py, "id", self.id).unwrap();
        dict.set_item(py, "pos", self.pos).unwrap();
        dict
    }
}

impl LineupArray {
    fn new(max_salary: usize, pos_counts: &PosCounts) -> Self {
        let scores_shape = (
            max_salary + 1,
            (pos_counts[0] + 1) as usize,
            (pos_counts[1] + 1) as usize,
            (pos_counts[2] + 1) as usize,
            (pos_counts[3] + 1) as usize,
            (pos_counts[4] + 1) as usize,
        );

        let players_shape = vec![
            max_salary + 1,
            (pos_counts[0] + 1) as usize,
            (pos_counts[1] + 1) as usize,
            (pos_counts[2] + 1) as usize,
            (pos_counts[3] + 1) as usize,
            (pos_counts[4] + 1) as usize,
            pos_counts.clone().iter().sum::<i32>() as usize,
        ];
        Self {
            scores: Array6::zeros(scores_shape),
            players: ArrayD::zeros(IxDyn(&players_shape)),
        }
    }
}

fn player_idx(pos: usize, place: &[usize]) -> usize {
    pos * 2 + (place[pos as usize])
}

fn scarce_player_idx(available: usize, scarce: usize, place: &[usize]) -> usize {
    let idx: usize = if scarce > available {
        // Scarce: 2, available: 1
        available
    } else {
        // Scarce: 1, available: 2
        available - 1
    };
    available * 2 + (place[idx])
}

fn handle_single_pos(player: &Player, write: &mut LineupArray, read: &LineupArray) {
    write.players.assign(&read.players);
    // Ignore everything below the player's salary since we know it won't
    // be affected.
    let ss = s![..player.salary, .., .., .., .., ..];
    write.scores.slice_mut(ss).assign(&read.scores.slice(ss));

    // Copy cells where player can't be chosen because the position
    // is unavailable
    let cs = copy_slice(player.salary, player.pos[0]);
    write.scores.slice_mut(cs).assign(&read.scores.slice(cs));

    let leave_slice = leave_slice(player.salary, player.pos[0]);
    let mut target = write.scores.slice_mut(scs(leave_slice.clone()));
    let mut target_players = write.players.slice_mut(ps(leave_slice.clone()));
    let leave = read.scores.slice(scs(leave_slice.clone()));
    // let leave_players = read.players.slice(ps(leave_slice.clone()));
    let take = read
        .scores
        .slice(scs(take_slice(player.salary, player.pos[0])));
    let take_players = read
        .players
        .slice(ps(take_slice(player.salary, player.pos[0])));

    azip!((index (s, pg, sg, sf, pf, c), o in &mut target, &l in &leave, &t in take) {
        let with_player = t + player.pred;
        *o = if l > with_player {
            l
        } else {
            let mut op = target_players.slice_mut(s![s, pg, sg, sf, pf, c, ..]);
            let tp = take_players.slice(s![s, pg, sg, sf, pf, c, ..]);
            op.assign(&tp);
            op[player_idx(player.pos[0], &[pg, sg, sf, pf, c])] = player.id + 1;

            with_player
        };
    });
}

fn handle_scarce_pos(
    player: &Player,
    scarce: usize,
    available: usize,
    write: &mut LineupArray,
    read: &LineupArray,
) {
    let salary = player.salary;
    let mut ts: Vec<SliceInfoElem> = (0..5)
        .map(|p| {
            if p == scarce {
                SliceInfoElem::Index(0)
            } else if p == available {
                SliceInfoElem::from(..(-1 as i32))
            } else {
                SliceInfoElem::from(..)
            }
        })
        .collect();

    let mut ls = ts.clone();
    ls[available] = SliceInfoElem::from((1 as i32)..);
    ts.insert(0, SliceInfoElem::from(..-salary));
    ls.insert(0, SliceInfoElem::from(salary..));

    let mut target = write.scores.slice_mut(single_slice(ls.clone()));
    let mut target_players = write.players.slice_mut(ps(ls.clone()));
    let leave = read.scores.slice(single_slice(ls.clone()));
    // let leave_players = read.players.slice(ps(leave_slice.clone()));
    let take = read.scores.slice(single_slice(ts.clone()));
    let take_players = read.players.slice(ps(ts.clone()));

    azip!((index (s, a, b, c, d), o in &mut target, &l in &leave, &t in take) {
        let with_player = t + player.pred;
        *o = if l > with_player {
            l
        } else {
            let mut op = target_players.slice_mut(s![s, a, b, c, d, ..]);
            let tp = take_players.slice(s![s, a, b,  c, d, ..]);
            op.assign(&tp);
            op[scarce_player_idx(available, scarce,  &[a, b, c, d])] = player.id + 1;

            with_player
        };
    });
}

fn handle_double_pos(player: &Player, write: &mut LineupArray, read: &LineupArray) {
    write.players.assign(&read.players);

    // Ignore everything below the player's salary since we know it won't
    // be affected.
    let ss = s![..player.salary, .., .., .., .., ..];
    write.scores.slice_mut(ss).assign(&read.scores.slice(ss));

    // Copy cells where player can't be chosen because both positions
    // is unavailable
    let cs = copy_slice2(player.salary, player.pos[0], player.pos[1]);
    write.scores.slice_mut(cs).assign(&read.scores.slice(cs));

    // Run 2D logic when one position is scarce
    handle_scarce_pos(player, player.pos[0], player.pos[1], write, read);
    handle_scarce_pos(player, player.pos[1], player.pos[0], write, read);

    // Main logic loop
    let leave_slice = leave_slice2(player.salary, player.pos[0], player.pos[1]);
    let mut target = write.scores.slice_mut(scs(leave_slice.clone()));
    let mut target_players = write.players.slice_mut(ps(leave_slice.clone()));
    let leave = read.scores.slice(scs(leave_slice.clone()));
    // let leave_players = read.players.slice(ps(leave_slice.clone()));
    let take = read.scores.slice(scs(take_slice2(
        player.salary,
        player.pos[0],
        player.pos[1],
    )));
    let take_players =
        read.players
            .slice(ps(take_slice2(player.salary, player.pos[0], player.pos[1])));

    // Second position
    let take_second = read.scores.slice(scs(take_slice2(
        player.salary,
        player.pos[1],
        player.pos[0],
    )));
    let take_players_second =
        read.players
            .slice(ps(take_slice2(player.salary, player.pos[1], player.pos[0])));

    azip!((index (s, pg, sg, sf, pf, c), o in &mut target, &l in &leave, &t in take, &t2 in take_second) {
        let with_player = t + player.pred;
        let with_player_second = t2 + player.pred;
        *o = if l > with_player && l > with_player_second {
            l
        } else if with_player > with_player_second {
            let mut op = target_players.slice_mut(s![s, pg, sg, sf, pf, c, ..]);
            let tp = take_players.slice(s![s, pg, sg, sf, pf, c, ..]);
            op.assign(&tp);
            op[player_idx(player.pos[0], &[pg, sg, sf, pf, c])] = player.id + 1;

            with_player
        } else {
            let mut op = target_players.slice_mut(s![s, pg, sg, sf, pf, c, ..]);
            let t2p = take_players_second.slice(s![s, pg, sg, sf, pf, c, ..]);
            op.assign(&t2p);
            op[player_idx(player.pos[1], &[pg, sg, sf, pf, c])] = player.id + 1;
            with_player_second

        };
    });
}

fn load_output(
    players: &[Player],
    pos_counts: &PosCounts,
    results: &ArrayView1<i32>,
) -> Vec<OutputPlayer> {
    let mut output: Vec<OutputPlayer> = Vec::new();
    let mut i: usize = 0;
    for (pos, count) in pos_counts.iter().enumerate() {
        for _ in 0..*count {
            if results[i] > 0 {
                output.push(OutputPlayer {
                    id: players[(results[i] - 1) as usize].id,
                    pos: pos as i32,
                });
            }
            i += 1;
        }
    }
    output
}

fn select_best(
    players: Vec<Player>,
    max_salary: usize,
    pos_counts: PosCounts,
) -> (Vec<OutputPlayer>, f32) {
    let mut lineup_players: Vec<Player> = players
        .iter()
        .enumerate()
        .map(|(idx, p)| Player {
            id: idx as i32,
            pos: p.pos.clone(),
            pred: p.pred,
            salary: p.salary,
        })
        .collect();
    lineup_players.sort_by(|a, b| b.pred.partial_cmp(&a.pred).unwrap());

    let mut read = LineupArray::new(max_salary, &pos_counts);
    let mut write = LineupArray::new(max_salary, &pos_counts);

    for player in &lineup_players {
        match player.pos.len() {
            1 => handle_single_pos(player, &mut write, &read),
            2 => handle_double_pos(player, &mut write, &read),
            _ => panic!("Player with unexpected pos count found"),
        }
        let mut at = pos_counts.clone();
        at[player.pos[0]] -= 1;

        mem::swap(&mut write, &mut read);
    }

    let best_score = read.scores[[
        max_salary,
        pos_counts[0] as usize,
        pos_counts[1] as usize,
        pos_counts[2] as usize,
        pos_counts[3] as usize,
        pos_counts[4] as usize,
    ]];
    let output = load_output(
        players.as_slice(),
        &pos_counts,
        &read.players.slice(s![
            max_salary,
            pos_counts[0],
            pos_counts[1],
            pos_counts[2],
            pos_counts[3],
            pos_counts[4],
            ..
        ]),
    );

    (output, best_score)
}

fn select_best_lineup(
    py: Python,
    players: Vec<Player>,
    max_salary: usize,
    pos_counts: PosCounts,
) -> PyResult<PyTuple> {
    Ok(select_best(players, max_salary, pos_counts).to_py_object(py))
}

#[cfg(test)]
mod tests {

    use rand::{thread_rng, Rng};

    use crate::{player::Player, select_best, OutputPlayer};

    // None: {
    //     "PG": {None: [5]},
    //     "SG": {None: [6]},
    //     "SF": {None: [7]},
    //     "PF": {None: [1, 2, 3]},
    //     "C": {None: [4]},
    //     "MaxSalary": {None: 60000},
    //     "s": {1: 20000, 2: 15000, 3: 5000, 4: 2000, 5: 1000, 6: 1000, 7: 1000},
    //     "u": {1: 30.0, 2: 25.0, 3: 15.0, 4: 20.0, 5: 1.0, 6: 1.0, 7: 1.0},
    // },

    #[test]
    fn basic_selection() {
        let players = vec![
            Player {
                id: 1,
                pos: vec![3],
                salary: 200,
                pred: 30.0,
            },
            Player {
                id: 2,
                pos: vec![3],
                salary: 150,
                pred: 25.0,
            },
            Player {
                id: 3,
                pos: vec![3],
                salary: 50,
                pred: 15.0,
            },
            Player {
                id: 4,
                pos: vec![4],
                salary: 20,
                pred: 20.0,
            },
        ];

        let (selected, total) = select_best(players, 600, vec![2, 2, 2, 2, 1]);
        assert_eq!(total, 75.0);
        assert_eq!(
            selected,
            vec![
                OutputPlayer { id: 1, pos: 3 },
                OutputPlayer { id: 2, pos: 3 },
                OutputPlayer { id: 4, pos: 4 }
            ]
        );
    }

    #[test]
    fn double_selection() {
        let players = vec![
            Player {
                id: 1,
                pos: vec![2],
                salary: 200,
                pred: 30.0,
            },
            Player {
                id: 2,
                pos: vec![2, 3],
                salary: 150,
                pred: 35.0,
            },
            Player {
                id: 3,
                pos: vec![2],
                salary: 50,
                pred: 25.0,
            },
            Player {
                id: 4,
                pos: vec![3],
                salary: 20,
                pred: 20.0,
            },
            Player {
                id: 5,
                pos: vec![3],
                salary: 20,
                pred: 15.0,
            },
        ];

        // 35 + 65
        let (selected, total) = select_best(players, 600, vec![2, 2, 2, 2, 1]);
        assert_eq!(total, 110.0);
        assert_eq!(
            selected,
            vec![
                OutputPlayer { id: 1, pos: 2 },
                OutputPlayer { id: 3, pos: 2 },
                OutputPlayer { id: 2, pos: 3 },
                OutputPlayer { id: 4, pos: 3 }
            ]
        );
    }

    fn players(count: usize) -> Vec<Player> {
        let mut rng = thread_rng();
        (0..count)
            .map(|i| {
                let pos = rng.gen_range(0..5);
                let pos2 = (pos + rng.gen_range(1..4)) % 5;
                Player {
                    id: i as i32,
                    pos: vec![pos, pos2],
                    pred: rng.gen::<f32>() * 50.0,
                    salary: rng.gen_range(36..601),
                }
            })
            .collect()
    }

    #[test]
    fn load_test() {
        select_best(players(347), 600, vec![2, 2, 2, 2, 1]);
    }

    // #[test]
    // fn array_tests() {
    //     let mut a = Array3::zeros((3, 3, 3));
    //     let mut b = Array3::zeros((3, 3, 3));
    //     a[[1, 1, 2]] = 4;
    //     let slice_args: SliceInfo<[SliceInfoElem; 3], Ix3, Ix1> = SliceInfo::try_from([
    //         SliceInfoElem::Index(1),
    //         SliceInfoElem::Index(1),
    //         SliceInfoElem::from(..),
    //     ])
    //     .unwrap();
    //     b.slice_mut(slice_args).assign(&a.slice(s![1, 1, ..]));
    //     assert_eq!(b[[1, 1, 2]], 4);
    // }
}
