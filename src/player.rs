use cpython::{FromPyObject, PyDict, PyObject, PyResult, Python};

#[derive(Clone)]
pub struct Player {
    pub id: i32,
    pub pos: Vec<usize>,
    pub pred: f32,
    pub salary: i32,
}

impl<'s> FromPyObject<'s> for Player {
    fn extract(py: Python, obj: &'s PyObject) -> PyResult<Self> {
        let dict = obj.extract::<PyDict>(py)?;
        Ok(Player {
            id: dict.get_item(py, "id").unwrap().extract(py)?,
            pos: dict.get_item(py, "pos").unwrap().extract(py)?,
            pred: dict.get_item(py, "pred").unwrap().extract(py)?,
            salary: dict.get_item(py, "salary").unwrap().extract(py)?,
        })
    }
}
