FROM rust:1.58-buster as build

WORKDIR /usr/app

COPY . .

RUN cargo build --release

FROM python:3.7-buster

COPY --from=build /usr/app/target/release/liblineup.so /lib/lineup.so

